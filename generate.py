import argparse
import json
import os

from media.processors import ArtProcessor, PhotoProcessor
from media.constants import ART_STORAGE_MANIFEST_FILE, PHOTOS_STORAGE_MANIFEST_FILE, PROJECT_PATH
from media.storage import StorageConfig
from media.utils import create_file_if_not_exists


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--art", action="store_true", dest="process_art", help="process art files")
    parser.add_argument("--photos", action="store_true", dest="process_photos", help="process photo files")
    parser.add_argument("--manifest-only", action="store_true",
                        help="generates the frontend manifest file without uploading files")
    args = parser.parse_args()

    storage_config = StorageConfig.from_file(os.path.join(PROJECT_PATH, "keys.yml"))

    print(
        "current project settings\n"
        f"PROJECT_PATH: {PROJECT_PATH}\n"
        f"BUCKET_NAME: {storage_config.bucket_name}\n"
        f"REGION_NAME: {storage_config.region_name}\n"
    )

    if not input("continue? (Y/N): ").upper().startswith("Y"):
        print("exiting")
        raise SystemExit

    kwargs = {
        "upload_files": not args.manifest_only,
        "storage_config": storage_config,
    }

    # TODO: create empty frontend manifest files
    create_file_if_not_exists(ART_STORAGE_MANIFEST_FILE, json.dumps([]))
    create_file_if_not_exists(PHOTOS_STORAGE_MANIFEST_FILE, json.dumps([]))

    if args.process_art:
        ArtProcessor(**kwargs).run()

    if args.process_photos:
        PhotoProcessor(**kwargs).run()
