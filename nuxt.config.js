export default {
  css: [
    "normalize.css",
    "@/assets/styles.scss",
  ],
  generate: {
    nojekyll: false,
  },
  head: {
    title: "Doktor",
    meta: [
      { charset: "utf-8" },

      { name: "author", content: "Doktor" },
      { name: "description", content: "The personal website of a blue dog." },
      { name: "viewport", content: "width=device-width, initial-scale=1" },

      { property: "og:title", content: "Doktor" },
      { property: "og:site_name", content: "Doktor" },
      {
        property: "og:description",
        content: "The personal website of a blue dog.",
      },

      { property: "og:image", content: "/favicon.png" },
      { property: "og:image:type", content: "image/png" },
      { property: "og:image:width", content: "512" },
      { property: "og:image:height", content: "512" },

      { property: "og:type", content: "profile" },
      { property: "profile:first_name", content: "Doktor" },
      { property: "profile:username", content: "Doktor" },
      { property: "og:url", content: "https://doktorthehusky.com" },
    ],
    script: [
      {
        defer: true,
        "data-domain": "doktorthehusky.com",
        src: "https://plausible.io/js/script.js",
      },
    ],
    link: [
      {
        rel: "icon",
        type: "image/png",
        sizes: "256x256",
        href: "/favicon.png",
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Roboto:400,700",
      },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Work+Sans:wght@700&display=swap",
      },
    ],

  },
  target: "static",
};
