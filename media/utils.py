import datetime
import glob
import os
import re
import unicodedata
from io import BytesIO
from PIL import Image
from typing import Iterable, Optional, Tuple

from media.constants import PROJECT_PATH, THUMBNAIL_SIZE


def create_file_if_not_exists(path: str, contents: str) -> None:
    try:
        with open(path, "x") as f:
            f.write(contents)
    except FileExistsError:
        pass


def filter_dir(pattern: re.Pattern, prefix: str = '') -> Iterable[str]:
    for name in os.listdir(prefix):
        if os.path.isfile(os.path.join(prefix, name)) and pattern.fullmatch(name) is not None:
            yield os.path.join(prefix, name)


def fit_image(image: Image, size: Tuple[int, int]) -> Image:
    if image.format != 'JPEG':
        image = image.convert('RGB')

    old_w, old_h = image.size
    old_ratio = old_w / old_h

    new_w, new_h = size
    new_ratio = new_w / new_h

    # Resize from portrait -> square/landscape: crop top and bottom
    if new_ratio > old_ratio:
        scale = new_w / old_w
        new_h = new_h / scale

        top = (old_h - new_h) / 2
        bottom = top + new_h

        image = image.crop(int_all(0, top, old_w, bottom))

    # Resize from landscape -> square/portrait: crop left and right
    elif new_ratio < old_ratio:
        scale = new_h / old_h
        new_w = new_w / scale

        left = (old_w - new_w) / 2
        right = left + new_w

        image = image.crop(int_all(left, 0, right, old_h))

    return image.resize(size, Image.LANCZOS)


def generate_thumbnail(file_path: str, size: int = THUMBNAIL_SIZE) -> Image:
    image = Image.open(file_path)

    if image.mode != "RGB":
        background = Image.new("RGB", image.size, (255, 255, 255))
        background.paste(image, mask=image.split()[-1])
        image = background

    if image.width < THUMBNAIL_SIZE or image.height < THUMBNAIL_SIZE:
        print(
            f"warning, image file is too small for thumbnail: "
            f"\"{os.path.relpath(file_path, start=PROJECT_PATH)}\" ({image.width}x{image.height})")

    return fit_image(image, (size, size))


def glob_single(*patterns: str, prefix: str = '') -> Optional[str]:
    results = sum([glob.glob(os.path.join(prefix, pattern)) for pattern in patterns], [])
    return results[0] if len(results) == 1 else None


def int_all(*nums: float) -> Iterable[int]:
    return (int(n) for n in nums)


def resize_image(file_path: str, long_edge: int) -> Image:
    image = Image.open(file_path)

    if image.width < long_edge and image.height < long_edge:
        print(
            f"warning, image file is too small: "
            f"\"{os.path.relpath(file_path, start=PROJECT_PATH)}\" ({image.width}x{image.height})")

    image.thumbnail((long_edge, long_edge))
    return image


def save_image_to_stream(image: Image, **kwargs) -> BytesIO:
    file = BytesIO()
    image.save(file, **kwargs)
    file.seek(0)

    return file


def serialize(o):
    if isinstance(o, (datetime.datetime, datetime.date)):
        return o.isoformat()

    raise TypeError(f"type {type(o)} is not serializable")


def slugify(value):
    value = unicodedata.normalize("NFKD", value).encode("ascii", "ignore").decode("ascii")
    value = re.sub("[^\w\s-]", "", value).strip().lower()
    return re.sub("[-\s]+", "-", value)
