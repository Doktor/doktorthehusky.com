import os

PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ART_DIRECTORY = os.path.join(PROJECT_PATH, "media", "art")
ART_STORAGE_MANIFEST_FILE = os.path.join(PROJECT_PATH, "media", "manifest_art.json")
ART_FRONTEND_MANIFEST_FILE = os.path.join(PROJECT_PATH, "static", "manifest_art.json")
ART_LONG_EDGE = 3840
ART_MANIFEST_FIELDS = ("artist", "title", "description", "type", "medium", "tags", "rating", "dates.done")

PHOTOS_DIRECTORY = os.path.join(PROJECT_PATH, "media", "photos")
PHOTOS_STORAGE_MANIFEST_FILE = os.path.join(PROJECT_PATH, "media", "manifest_photos.json")
PHOTOS_FRONTEND_MANIFEST_FILE = os.path.join(PROJECT_PATH, "static", "manifest_photos.json")
PHOTO_LONG_EDGE = 2000
PHOTOS_MANIFEST_FIELDS = ("photographer", "title", "description", "date", "location", "tags", "rating")

THUMBNAIL_SIZE = 600
THUMBNAIL_FORMAT = "JPEG"

DELIMITER = "/"


def get_key(*parts: str) -> str:
    return DELIMITER.join(parts)


KEY_PREFIX = get_key("doktorthehusky.com", "images")
FULL_SIZE_PREFIX = "full"
THUMBNAIL_PREFIX = "thumbnails"
