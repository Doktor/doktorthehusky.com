import abc
import glob
import hashlib
import json
import os
import re
import yaml
from io import BytesIO
from PIL import Image
from typing import Any, Iterable, List, Optional, Tuple

from media.constants import (
    ART_DIRECTORY,
    ART_FRONTEND_MANIFEST_FILE,
    ART_LONG_EDGE,
    ART_MANIFEST_FIELDS,
    ART_STORAGE_MANIFEST_FILE,
    KEY_PREFIX,
    PHOTOS_DIRECTORY,
    PHOTOS_FRONTEND_MANIFEST_FILE,
    PHOTOS_MANIFEST_FIELDS,
    PHOTOS_STORAGE_MANIFEST_FILE,
    PHOTO_LONG_EDGE,
    THUMBNAIL_FORMAT,
    FULL_SIZE_PREFIX,
    THUMBNAIL_PREFIX,
    get_key)
from media.storage import DigitalOceanSpacesClient, StorageClient, StorageConfig
from media.utils import filter_dir, generate_thumbnail, glob_single, resize_image, save_image_to_stream, serialize, slugify


class Processor(abc.ABC):
    def __init__(
            self,
            s3_manifest_file: str,
            frontend_manifest_file: str,
            manifest_fields: Iterable[str],
            content_directory: str,
            key_prefix: str,
            image_long_edge: int,
            storage_config: StorageConfig = None,
            upload_files: bool = True):

        self.items = []

        # Paths
        self.s3_manifest_file = s3_manifest_file
        self.frontend_manifest_file = frontend_manifest_file
        self.content_directory = content_directory

        # Object storage
        self.storage: StorageClient = DigitalOceanSpacesClient(storage_config)
        self.full_size_prefix = get_key(key_prefix, FULL_SIZE_PREFIX)
        self.thumbnail_prefix = get_key(key_prefix, THUMBNAIL_PREFIX)

        # Change detection
        self.new_manifest: List[str] = []
        with open(self.s3_manifest_file, "r") as f:
            self.old_manifest: List[str] = json.loads(f.read().strip())

        # Other settings
        self.manifest_fields = manifest_fields
        self.long_edge = image_long_edge
        self.upload_files = upload_files

        # Counters
        self.ok = 0
        self.already_processed = 0
        self.no_details = 0
        self.no_images = 0

    @staticmethod
    def get_field(pairs: dict, field: str) -> Optional[Any]:
        value = None

        for part in field.split("."):
            value = pairs.get(part, None)

            if value is None:
                return None

            pairs = value

        return value

    @staticmethod
    def get_file_hash(path: str) -> str:
        with open(path, "rb") as f:
            hasher = hashlib.md5()

            while chunk := f.read(128 * 64):
                hasher.update(chunk)

            return hasher.hexdigest()

    @staticmethod
    def get_item_details(path: str) -> Optional[dict]:
        details_path = glob_single("*.yml", prefix=path)

        if details_path is None:
            return None

        with open(details_path, "r") as f:
            return yaml.safe_load(f.read().strip())

    def get_item_images(self, path: str) -> Iterable[str]:
        raise NotImplementedError

    def get_items(self) -> Iterable[str]:
        return [item.path for item in os.scandir(self.content_directory) if item.is_dir()]

    def get_url(self, key: str) -> str:
        return self.storage.get_url(key)

    def process_image(self, image_path: str) -> Tuple[str, str]:
        # Calculate file hash
        md5 = self.get_file_hash(image_path)
        self.new_manifest.append(md5)

        # Get object keys
        ext = os.path.splitext(image_path)[1]
        resized_key = get_key(self.full_size_prefix, f"{md5}{ext}")
        thumbnail_key = get_key(self.thumbnail_prefix, f"{md5}.jpg")
        keys = (resized_key, thumbnail_key)

        # If we're not uploading any files, return the object keys now
        if not self.upload_files:
            return keys

        # Generate resized image
        resized_image = resize_image(image_path, self.long_edge)
        resized_file = save_image_to_stream(resized_image, format=resized_image.format, quality=80, optimize=True)

        if md5 in self.old_manifest:
            print(f"image previously processed: {md5}")
            self.already_processed += 1
            return keys

        # Upload resized image
        self.upload_image(resized_file, resized_key, resized_image.format)

        # Generate thumbnail
        thumbnail_image = generate_thumbnail(image_path)
        thumbnail_file = save_image_to_stream(thumbnail_image, format=THUMBNAIL_FORMAT, quality=80, optimize=True)

        # Upload thumbnail
        self.upload_image(thumbnail_file, thumbnail_key, THUMBNAIL_FORMAT)

        return keys

    def process_item(self, path: str):
        # Process details file
        details = self.get_item_details(path)

        if details is None:
            print(f"skipping, no details file: \"{path}\"")
            self.no_details += 1
            return

        # Process image files
        resized_keys = []
        thumbnail_keys = []
        item_image_count = 0

        for image_path in self.get_item_images(path):
            resized_key, thumbnail_key = self.process_image(image_path)
            resized_keys.append(resized_key)
            thumbnail_keys.append(thumbnail_key)

            item_image_count += 1
            self.ok += 1

            print(f"successfully processed file: {image_path}")

        if not item_image_count:
            print(f"skipped, no image files: \"{path}\"")
            self.no_images += 1
            return

        self.process_object_keys(details, zip(resized_keys, thumbnail_keys))

    def process_items(self):
        for path in self.get_items():
            self.process_item(path)

        if self.upload_files:
            with open(self.s3_manifest_file, "w") as f:
                f.write(json.dumps(self.new_manifest))

        with open(self.frontend_manifest_file, "w") as f:
            f.write(json.dumps(self.items, indent=2, default=serialize))

        print(
            "\n"
            "finished processing\n"
            f"{self.ok} items processed without issue\n"
            f"{self.already_processed} items previously processed\n"
            f"{self.no_details} items without details\n"
            f"{self.no_images} items without images\n"
        )

    def process_object_keys(self, details: dict, keys: Iterable[Tuple[str, str]]) -> None:
        raise NotImplementedError

    def run(self):
        self.process_items()

    def upload_image(self, image_file: BytesIO, key: str, image_format: str) -> None:
        self.storage.upload_file(image_file, key, Image.MIME[image_format])


class ArtProcessor(Processor):
    filename_pattern = re.compile(r"original(_[0-9]+)?\.(jpg|png)", re.IGNORECASE)

    def __init__(self, storage_config: StorageConfig, upload_files: bool):
        super().__init__(
            ART_STORAGE_MANIFEST_FILE,
            ART_FRONTEND_MANIFEST_FILE,
            ART_MANIFEST_FIELDS,
            ART_DIRECTORY,
            get_key(KEY_PREFIX, "art"),
            ART_LONG_EDGE,
            storage_config=storage_config,
            upload_files=upload_files)

    def get_item_images(self, path: str) -> Iterable[str]:
        return filter_dir(self.filename_pattern, prefix=path)

    def process_object_keys(self, details: dict, keys: Iterable[Tuple[str, str]]) -> None:
        for resized_key, thumbnail_key in keys:
            self.items.append({
                **{field: self.get_field(details, field) for field in ART_MANIFEST_FIELDS},
                "image": self.get_url(resized_key),
                "thumbnail": self.get_url(thumbnail_key),
            })


class PhotoProcessor(Processor):
    def __init__(self, storage_config: StorageConfig, upload_files: bool):
        super().__init__(
            PHOTOS_STORAGE_MANIFEST_FILE,
            PHOTOS_FRONTEND_MANIFEST_FILE,
            PHOTOS_MANIFEST_FIELDS,
            PHOTOS_DIRECTORY,
            get_key(KEY_PREFIX, "photos"),
            PHOTO_LONG_EDGE,
            storage_config=storage_config,
            upload_files=upload_files)

    def get_item_images(self, path: str) -> Iterable[str]:
        return glob.glob(os.path.join(path, "*.jpg"))

    def process_object_keys(self, item_details: dict, keys: Iterable[Tuple[str, str]]) -> None:
        self.items.append({
            **{field: self.get_field(item_details, field) for field in self.manifest_fields},
            "slug": slugify(item_details.get("title", "")),
            "images": [
                {
                    "image": self.get_url(resized_key),
                    "thumbnail": self.get_url(thumbnail_key),
                }
                for resized_key, thumbnail_key in keys
            ],
        })
