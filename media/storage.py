import abc
import boto3
import dataclasses
import yaml
from io import BytesIO


@dataclasses.dataclass
class StorageConfig:
    bucket_name: str
    region_name: str
    access_key: str
    secret_key: str

    @classmethod
    def from_file(cls, path: str) -> "StorageConfig":
        with open(path, "r") as f:
            data = yaml.safe_load(f.read().strip())
            fields = [field.name for field in dataclasses.fields(StorageConfig)]
            return StorageConfig(*(data.get(field, None) for field in fields))


class StorageClient(abc.ABC):
    def __init__(self, config: StorageConfig, endpoint_url: str = ""):
        self.bucket_name = config.bucket_name
        self.region_name = config.region_name

        kwargs = {
            "aws_access_key_id": config.access_key,
            "aws_secret_access_key": config.secret_key,
        }

        if config.region_name:
            kwargs["region_name"] = config.region_name

        if endpoint_url:
            kwargs["endpoint_url"] = endpoint_url

        self.client = boto3.client("s3", **kwargs)

    def get_url(self, key: str) -> str:
        raise NotImplementedError

    def upload_file(self, file: BytesIO, key: str, content_type: str):
        args = {"ACL": "public-read", "ContentType": content_type}
        self.client.upload_fileobj(file, self.bucket_name, key, ExtraArgs=args)


class S3Client(StorageClient):
    def __init__(self, config: StorageConfig):
        super().__init__(config, endpoint_url=f"https://{config.bucket_name}.s3.{config.region_name}.amazonaws.com")

    def get_url(self, key: str) -> str:
        return f"https://{self.bucket_name}.s3.{self.region_name}.amazonaws.com/{key}"


class DigitalOceanSpacesClient(StorageClient):
    def __init__(self, config: StorageConfig):
        super().__init__(config, endpoint_url=f"https://{config.region_name}.digitaloceanspaces.com")

    def get_url(self, key: str) -> str:
        return f"https://{self.bucket_name}.{self.region_name}.digitaloceanspaces.com/{key}"
