# doktorthehusky.com

## About

This is the repository for my personal website, [doktorthehusky.com](https://doktorthehusky.com).

It's built using Nuxt.js and Vue and intended to be deployed as a static site.

## Instructions

**Start the development server**

`npm run dev`

The server will be accessible at `http://localhost:3000`.

**Build for production**

`npm run build`

The static files will be placed `/dist/`.
